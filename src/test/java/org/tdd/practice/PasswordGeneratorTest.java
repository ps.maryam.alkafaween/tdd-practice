package org.tdd.practice;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.tdd.practice.util.*;

import java.util.*;

import static java.lang.Character.isDigit;
import static org.junit.jupiter.api.Assertions.*;
import static org.tdd.practice.util.TypeChecker.*;
import static org.tdd.practice.util.AdjacencyChecker.*;
import static org.tdd.practice.util.TypeConverter.stringToList;

public class PasswordGeneratorTest {
    private PasswordGenerator passwordGenerator;
    private String password;

    @BeforeEach
    public void setup() {
        passwordGenerator = new PasswordGenerator();
        password = passwordGenerator.generatePassword();
    }

    @Test
    public void givenPassword_whenLength_thenEightIsReturned() {
        assertEquals(8, password.length());
    }

    @Test
    public void givenPassword_whenCountingDigitsLettersSymbols_thenFourTwoTwoAreReturned() {
        int digitsCounter = 0, upperCaseCounter = 0;
        Map<Character, Integer> symbolsCounter = new HashMap<>();
        for (Symbol symbol : Symbol.values()) {
            symbolsCounter.put(symbol.getVal(), 0);
        }
        for (int i = 0; i < password.length(); i++) {
            char curChar = password.charAt(i);
            digitsCounter += isDigit(curChar) ? 1 : 0;
            upperCaseCounter += (Character.isUpperCase(curChar)) ? 1 : 0;
            if (isSymbol(curChar))
                incrementFreqOfChar(symbolsCounter, curChar);
        }
        int totalSymbols = getTotalUniqueUsedSymbols(symbolsCounter);
        assertEquals(4, digitsCounter);
        assertEquals(2, upperCaseCounter);
        assertEquals(2, totalSymbols);
    }

    @Test
    public void givenPassword_whenLetterAdjacencyTested_thenFalseIsReturned() {
        List <Character> passwordAsList = stringToList(password);
        assertFalse(adjacentLettersExist(passwordAsList));
    }
    @Test
    public void givenPassword_whenSymbolAdjacencyTested_thenFalseIsReturned() {
        List <Character> passwordAsList = stringToList(password);
        assertFalse(adjacentSymbolsExist(passwordAsList));
    }
    @Test
    public void givenPassword_whenDigitsAdjacencyTested_thenFalseIsReturned() {
        List <Character> passwordAsList = stringToList(password);
        assertFalse(adjacentDigitsExist(passwordAsList));
    }

    @Test
    public void givenSomeGeneratedPasswords_whenEqualityTested_thenFalseIsReturned() {
        Set<String> passwords = new HashSet<>();
        final int NUM_CALLS_TO_CHECK_RANDOMNESS = 5;
        boolean equalityExists = false;
        for (int i = 0; i < NUM_CALLS_TO_CHECK_RANDOMNESS; i++) {
            String newPassword = passwordGenerator.generatePassword();
            equalityExists |= passwords.contains(newPassword);
            passwords.add(passwordGenerator.generatePassword());
        }
        assertFalse(equalityExists);
    }

    private int getTotalUniqueUsedSymbols(Map<Character, Integer> symbolsCounter) {
        int totalSymbols = 0;
        for (Map.Entry<Character, Integer> entry : symbolsCounter.entrySet()) {
            totalSymbols += entry.getValue() > 0 ? 1 : 0;
        }
        return totalSymbols;
    }

    private void incrementFreqOfChar(Map<Character, Integer> symbolsCounter, char c) {
        symbolsCounter.put(c, symbolsCounter.get(c) + 1);
    }
}
