package org.tdd.practice;

public class Main {
    public static void main(String[] args) {
        String password = new PasswordGenerator().generatePassword();
        System.out.println("The randomly generated password is " + password);
    }
}