package org.tdd.practice.util;

public enum Symbol {
    HASH('#'),
    DOLLAR('$'),
    PERCENT('%'),
    UNDERSCORE('_');


    private final char val;
    public char getVal() {
        return val;
    }
    Symbol(char val) {
        this.val = val;
    }

}
