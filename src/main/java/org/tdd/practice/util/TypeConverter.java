package org.tdd.practice.util;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class TypeConverter {
    public static List<Character> stringToList(String password) {
        return password.chars().mapToObj(e -> (char) e).collect(toList());
    }
    public static String listToString(List<Character> passwordAsList) {
        StringBuilder password = new StringBuilder();
        for (char c: passwordAsList) {
            password.append(c);
        }
        return password.toString();
    }
}
