package org.tdd.practice.util;

import java.util.List;

import static org.tdd.practice.util.TypeChecker.*;

public class AdjacencyChecker {
    public static boolean adjacentLettersExist(List<Character> passwordAsList) {
        boolean adjacentLettersExist = false;
        for (int i = 0; i < passwordAsList.size()-1; i++) {
            adjacentLettersExist |= (areAllLetters(passwordAsList.get(i), passwordAsList.get(i + 1)));
        }
        return adjacentLettersExist;
    }
    public static boolean adjacentSymbolsExist(List <Character> passwordAsList) {
        boolean adjacentSymbolsExist = false;
        for (int i = 0; i < passwordAsList.size()-1; i++) {
            adjacentSymbolsExist |= (areAllSymbols(passwordAsList.get(i), passwordAsList.get(i + 1)));
        }
        return adjacentSymbolsExist;
    }
    public static boolean adjacentDigitsExist(List <Character> passwordAsList) {
        boolean adjacentDigitsExist = false;
        for (int i = 0; i < passwordAsList.size() - 2; i++) {
            char curChar = passwordAsList.get(i);
            char nextChar = passwordAsList.get(i + 1);
            char nextNextChar = passwordAsList.get(i + 2);
            adjacentDigitsExist |= areAllDigits(curChar, nextChar, nextNextChar);
        }
        return adjacentDigitsExist;
    }
}
