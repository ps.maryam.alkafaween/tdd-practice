package org.tdd.practice.util;

import static java.lang.Character.isDigit;
import static java.lang.Character.isLetter;

public class TypeChecker {
    public static boolean areAllDigits(char... args) {
        boolean areAllNumbers = true;
        for (char arg: args) {
            areAllNumbers &= isDigit(arg);
        }
        return areAllNumbers;
    }
    public static boolean areAllSymbols(char... args) {
        boolean areAllSymbols = true;
        for (char arg: args) {
            areAllSymbols &= isSymbol(arg);
        }
        return areAllSymbols;
    }
    public static boolean isSymbol(char c) {
        for (Symbol symbol : Symbol.values()) {
            if (symbol.getVal() == c) {
                return true;
            }
        }
        return false;
    }
    public static boolean areAllLetters(char... args) {
        boolean areAllLetters = true;
        for (char arg: args) {
            areAllLetters &= isLetter(arg);
        }
        return areAllLetters;
    }
}
