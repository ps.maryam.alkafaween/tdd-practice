package org.tdd.practice;

import org.tdd.practice.util.Symbol;

import java.util.*;

import static org.tdd.practice.util.AdjacencyChecker.*;
import static org.tdd.practice.util.TypeConverter.listToString;

public class PasswordGenerator {

    /*Using TDD, implement the following requirements:
Implement a random password generator which generates a password of 8 characters length with the following rules:
1- it should contains 4 numbers (0-9)
2- it should contains two upper case letters (A-Z)
3- it should contains two of the following symbols (_ $ # %)
4- the generated password characters should be shuffled without having each group of characters following each other
5- multiple calls for password generator shouldn't return the same password.*/

    private final List<Character> passwordAsList;
    private final Random random;

    public PasswordGenerator() {
        passwordAsList = new ArrayList<>();
        random = new Random();
    }

    public String generatePassword() {
        passwordAsList.clear();
        addRandNDigits(4);
        addRandNSymbols(2);
        addRandNCaps(2);
        while (!adjacencyConditionsFulfilled())
            shufflePassword();
        return listToString(passwordAsList);
    }

    private void addRandNDigits(int n) {
        for (int i = 0; i < n; i++) {
            passwordAsList.add(getRandDig());
        }
    }
    private char getRandDig() {
        final char UPPER_BOUND = '9';
        final char LOWER_BOUND = '0';
        return (char) (random.nextInt(UPPER_BOUND - LOWER_BOUND) + LOWER_BOUND);
    }
    private void addRandNSymbols(int n) {
        Set <Character> previousSymbols = new HashSet<>();
        for (int i = 0; i < n; i++) {
            char newSymbol = getRandSymbol();
            while (generatedSymbolBefore(newSymbol, previousSymbols)) {
                newSymbol = getRandSymbol();
            }
            passwordAsList.add(newSymbol);
            previousSymbols.add(newSymbol);
        }
    }
    private char getRandSymbol() {
        int randIndex = random.nextInt(4);
        return Symbol.values()[randIndex].getVal();
    }
    private void addRandNCaps(int n) {
        for (int i = 0; i < n; i++) {
            passwordAsList.add(getRandCap());
        }
    }
    private char getRandCap() {
        final char UPPER_BOUND = 'Z';
        final char LOWER_BOUND = 'A';
        return (char) (random.nextInt(UPPER_BOUND - LOWER_BOUND) + LOWER_BOUND);
    }
    private boolean generatedSymbolBefore(char symbol, Set<Character> previousSymbols) {
        return previousSymbols.contains(symbol);
    }
    private void shufflePassword() {
        Collections.shuffle(passwordAsList);
    }
    private boolean adjacencyConditionsFulfilled() {
        boolean conditionsFulfilled = true;
        conditionsFulfilled &= (!adjacentDigitsExist(passwordAsList));
        conditionsFulfilled &= (!adjacentLettersExist(passwordAsList));
        conditionsFulfilled &= (!adjacentSymbolsExist(passwordAsList));
        return conditionsFulfilled;
    }
}
